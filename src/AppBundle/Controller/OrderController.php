<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\RequestData;

class OrderController extends AbstractFOSRestController
{
	 /**
     * @Rest\Get("/order/{id}")
     */
    public function getAction($id)
    {
      	$restresult = $this->getDoctrine()->getRepository('AppBundle:RequestData')->find($id);
      	if ($restresult === null) {
            $response = new Response('{"err":"Invalid order"}');
            $response->headers->set('Content-Type', 'application/json');
            return $response;
     	}

     	$restresult->error = "";
     	//the encoding is done in the entity RequestData
      $response = new Response($restresult->getJson());
	    $response->headers->set('Content-Type', 'application/json');

		return $response;
    }
}