<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RequestData
 *
 * @ORM\Table(name="request_data")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RequestDataRepository")
 */
class RequestData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="delivery", type="date", nullable=true)
     */
    private $delivery;

    /**
     * @var bool
     *
     * @ORM\Column(name="isDelivered", type="boolean")
     */
    private $isDelivered;

    /**
     * @var string
     *
     * @ORM\Column(name="destination", type="string", length=100, nullable=true)
     */
    private $destination;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set delivery
     *
     * @param \DateTime $delivery
     *
     * @return RequestData
     */
    public function setDelivery($delivery)
    {
        $this->delivery = $delivery;

        return $this;
    }

    /**
     * Get delivery
     *
     * @return \DateTime
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * Set isDelivered
     *
     * @param boolean $isDelivered
     *
     * @return RequestData
     */
    public function setIsDelivered($isDelivered)
    {
        $this->isDelivered = $isDelivered;

        return $this;
    }

    /**
     * Get isDelivered
     *
     * @return bool
     */
    public function getIsDelivered()
    {
        return $this->isDelivered;
    }

    /**
     * Set destination
     *
     * @param string $destination
     *
     * @return RequestData
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * Get destination
     *
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Get the data as a json
     *
     * @return string
     */
    public function getJson(){
        return 
            json_encode([
                "id"=>$this->id,
                "delivery"=>$this->delivery,
                "isDelivered"=>$this->isDelivered,
                ]
            );
    }
}

