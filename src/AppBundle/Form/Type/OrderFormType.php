<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class OrderFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
             ->add('orderid', TextType::class,[
                'label'=>'Order Id',
                'attr'=>[
                    'class'=>'form-control col',
                    'placeholder'=>'Order Id',
                    'aria-describedby'=>'basic-addon1',
                    'aria-label'=>'Order Id'
                ]
            ])
            ->add('search', SubmitType::class,[
                'attr'=>['class'=>'btn btn-dark col col-lg-2']
            ])
        ;
    }
}